package org.example;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class App {

    private boolean            continueToCreate;
    private ThreadPoolExecutor executor;

    private static final int MAX_THREADS                      = 10;
    private static final int MAX_JOB_TIME                     = 100000;
    private static final int WAIT_TIME_FOR_NEW_JOB            = 1000;
    private static final int WAIT_TIME_TO_CHECK_THREAD_FINISH = 1000;

    public void setContinueToCreate(boolean value) {
        this.continueToCreate = value;
    }

    /**
     * Sleep for a given time in miliseconds
     *
     * @param waitTime Sleep time in miliseconds
     */
    private void waitForNewJob(int waitTime) {
        try {
            Thread.sleep(waitTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Continuously create new job and wait for a certain time
     * @param maxNumberOfThreads Maximum number of threads
     * @param maxJobTime Maximum time for a job
     * @param waitTime Wait time in miliseconds
     */
    private void createNewJobs(int maxNumberOfThreads, int maxJobTime, int waitTime) {
        int i = 1;
        while (continueToCreate) {
            if (executor.getActiveCount() >= maxNumberOfThreads) {
                System.out.println("Thread pool is full");
            } else {
                executor.execute(new Job(Integer.toString(i++), maxJobTime));
            }
            waitForNewJob(waitTime);
        }
    }

    /**
     * Initilization of service
     * @param maxNumberOfThreads Maximum number of threads
     * @param timeToCheckThreadFinish Time in miliseconds for checking if thread has been finished
     */
    private void initilizeService(int maxNumberOfThreads,
                                  int timeToCheckThreadFinish) {
        continueToCreate = true;
        executor         = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxNumberOfThreads);
        executor.setMaximumPoolSize(maxNumberOfThreads);

        Runtime.getRuntime().addShutdownHook(new ShutdownThread(this, executor, timeToCheckThreadFinish));
    }

    /**
     * Process
     */
    public void process() {
        initilizeService(MAX_THREADS, WAIT_TIME_TO_CHECK_THREAD_FINISH);
        createNewJobs(MAX_THREADS, MAX_JOB_TIME, WAIT_TIME_FOR_NEW_JOB);
    }

    public static void main(String[] args) {
        App app = new App();
        app.process();
    }

}
