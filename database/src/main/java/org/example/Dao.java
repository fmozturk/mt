package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class Dao {

    private Logger LOG = LoggerFactory.getLogger(Dao.class);

    private final String DATABASE_DATE_TIME_SQL = "SELECT CURRENT_TIMESTAMP";

    private final String driverClassname;
    private final String url;
    private final String username;
    private final String password;

    public Dao(String drivername, String url, String username, String password) {
        this.driverClassname = drivername;
        this.url = url;
        this.username = username;
        this.password = password;
    }

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(driverClassname);
        return DriverManager.getConnection(url, username, password);
    }

    public Timestamp getDatabaseDateTime() {
        Timestamp result = null;
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();) {
            ResultSet rs = stmt.executeQuery(DATABASE_DATE_TIME_SQL);
            if (rs.next())
                result = rs.getTimestamp(1);
        } catch (ClassNotFoundException e) {
            LOG.error(e.getMessage(), e);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return result;
    }

}
