package org.example;

import org.h2.tools.Server;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class App {

    private final Logger LOG = LoggerFactory.getLogger(App.class);

    private boolean            continueToCreate;
    private ThreadPoolExecutor executor;
    private Server             databaseServer;

    private final String PROPERTIES_FILENAME                       = "settings.file";
    private final String PROPERTY_MAX_THREADS                      = "maxThreads";
    private final String PROPERTY_MAX_JOB_TIME                     = "maxJobTime";
    private final String PROPERTY_WAIT_TIME_FOR_NEW_JOB            = "waitTimeForNewJob";
    private final String PROPERTY_WAIT_TIME_TO_CHECK_THREAD_FINISH = "waitTimeToCheckThreadFinish";
    private final String PROPERTY_JDBC_URL                         = "jdbc.url";
    private final String PROPERTY_JDBC_DRIVERNAME                  = "jdbc.drivername";
    private final String PROPERTY_JDBC_USERNAME                    = "jdbc.username";
    private final String PROPERTY_JDBC_PASSWORD                    = "jdbc.password";

    private int maxThreads                  = 10;
    private int maxJobTime                  = 100000;
    private int waitTimeForNewJob           = 1000;
    private int waitTimeToCheckThreadFinish = 1000;

    private String jdbcUrl;
    private String jdbcDrivername;
    private String jdbcUsername;
    private String jdbcPassword;

    public void setContinueToCreate(boolean value) {
        this.continueToCreate = value;
    }

    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    public void setMaxJobTime(int maxJobTime) {
        this.maxJobTime = maxJobTime;
    }

    public void setWaitTimeForNewJob(int waitTimeForNewJob) {
        this.waitTimeForNewJob = waitTimeForNewJob;
    }

    public void setWaitTimeToCheckThreadFinish(int waitTimeToCheckThreadFinish) {
        this.waitTimeToCheckThreadFinish = waitTimeToCheckThreadFinish;
    }

    public Server getDatabaseServer() {
        return databaseServer;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getJdbcDrivername() {
        return jdbcDrivername;
    }

    public void setJdbcDrivername(String jdbcDrivername) {
        this.jdbcDrivername = jdbcDrivername;
    }

    public String getJdbcUsername() {
        return jdbcUsername;
    }

    public void setJdbcUsername(String jdbcUsername) {
        this.jdbcUsername = jdbcUsername;
    }

    public String getJdbcPassword() {
        return jdbcPassword;
    }

    public void setJdbcPassword(String jdbcPassword) {
        this.jdbcPassword = jdbcPassword;
    }

    /**
     * Sleep for a given time in miliseconds
     *
     * @param waitTime Sleep time in miliseconds
     */
    private void waitForNewJob(int waitTime) {
        try {
            Thread.sleep(waitTime);
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * Continuously create new job and wait for a certain time
     *
     * @param maxNumberOfThreads Maximum number of threads
     * @param maxJobTime         Maximum time for a job
     * @param waitTime           Wait time in miliseconds
     */
    private void createNewJobs(int maxNumberOfThreads, int maxJobTime, int waitTime) {
        int i = 1;
        while (continueToCreate) {
            if (executor.getActiveCount() >= maxNumberOfThreads) {
                LOG.info("Thread pool is full");
            } else {
                executor.execute(new Job(
                        Integer.toString(i++),
                        maxJobTime,
                        getJdbcDrivername(),
                        getJdbcUrl(),
                        getJdbcUsername(),
                        getJdbcPassword()));
            }
            waitForNewJob(waitTime);
        }
    }

    /**
     * Initilization of service
     *
     * @param maxNumberOfThreads      Maximum number of threads
     * @param timeToCheckThreadFinish Time in miliseconds for checking if thread has been finished
     */
    private void initilizeService(int maxNumberOfThreads,
                                  int timeToCheckThreadFinish) {
        continueToCreate = true;
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxNumberOfThreads);
        executor.setMaximumPoolSize(maxNumberOfThreads);

        Runtime.getRuntime().addShutdownHook(new ShutdownThread(this, executor, timeToCheckThreadFinish));
    }

    /**
     * Check if property value is valid or not
     *
     * @param defaultValue Default value
     * @param propertyName Property name
     * @param properties   Properties object
     * @return
     * @throws Exception Throws if value is not integer
     */
    private int getPropertyIntegerValue(int defaultValue, String propertyName, Properties properties) throws Exception {
        int result = defaultValue;
        if (propertyName != null && properties != null && properties.getProperty(propertyName) != null) {
            try {
                result = Integer.parseInt(properties.getProperty(propertyName));
            } catch (NumberFormatException ex) {
                throw new Exception("Ayar dosyasindaki " + propertyName + " degiskeninde hatali deger");
            }
        }
        return result;
    }

    /**
     * Check for properties file name setting
     *
     * @param propertiesFilename
     * @return Name of properties file
     * @throws Exception
     */
    private String getValidFilename(String propertiesFilename) throws Exception {
        String result = null;
        if (propertiesFilename != null) {
            if (System.getProperty(PROPERTIES_FILENAME) != null) {
                throw new Exception("Ayni anda hem " + PROPERTIES_FILENAME + " degeri ve parametre ile dosya ismi verilemez!");
            } else {
                result = propertiesFilename;
            }
        } else {
            if (System.getProperty(PROPERTIES_FILENAME) != null)
                result = System.getProperty(PROPERTIES_FILENAME);
            else
                throw new Exception(PROPERTIES_FILENAME + " degeri veya parametre ile dosya ismi verilmelidir!");
        }
        return result;
    }

    /**
     * Checking for valid settings file and setting properties
     *
     * @param propertiesFilename
     * @throws Exception Throws exception if properties file can not be read
     */
    private void initilizeProperties(String propertiesFilename) throws Exception {
        String filename = getValidFilename(propertiesFilename);
        Properties properties = new Properties();

        try (FileInputStream is = new FileInputStream(filename)) {
            properties.load(is);
            setMaxThreads(getPropertyIntegerValue(maxThreads, PROPERTY_MAX_THREADS, properties));
            setMaxJobTime(getPropertyIntegerValue(maxJobTime, PROPERTY_MAX_JOB_TIME, properties));
            setWaitTimeForNewJob(getPropertyIntegerValue(waitTimeForNewJob, PROPERTY_WAIT_TIME_FOR_NEW_JOB, properties));
            setWaitTimeToCheckThreadFinish(getPropertyIntegerValue(waitTimeToCheckThreadFinish,
                    PROPERTY_WAIT_TIME_TO_CHECK_THREAD_FINISH,
                    properties));
            // Database configuration values
            setJdbcDrivername(properties.getProperty(PROPERTY_JDBC_DRIVERNAME));
            setJdbcUrl(properties.getProperty(PROPERTY_JDBC_URL));
            setJdbcUsername(properties.getProperty(PROPERTY_JDBC_USERNAME));
            setJdbcPassword(properties.getProperty(PROPERTY_JDBC_PASSWORD));
        } catch (IOException ex) {
            throw new Exception(filename + " dosyasini okurken sorun yasandi " + ex.toString());
        }
    }

    private void initilizeDatabase() throws SQLException {
        databaseServer = Server.createTcpServer(new String[]{});
        databaseServer.start();
    }

    /**
     * Process
     */
    public void process(String propertiesFilename) {
        try {
            initilizeProperties(propertiesFilename);
            initilizeDatabase();
            initilizeService(maxThreads, waitTimeToCheckThreadFinish);
            createNewJobs(maxThreads, maxJobTime, waitTimeForNewJob);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }


    public static void main(String[] args) {
        App app = new App();
        if (args.length > 0)
            app.process(args[0]);
        else
            app.process(null);
    }

}
