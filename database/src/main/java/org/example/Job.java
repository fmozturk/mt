package org.example;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.time.LocalDateTime;
import java.util.Random;

/**
 * A class that simulates a job,
 * prints job start time,
 * wait for a random of time,
 * prints job end time
 */
public class Job implements Runnable {

    private final Logger LOG = LoggerFactory.getLogger(Job.class);

    private final Random random = new Random();
    private final String name;
    private final int    maxWaitTime;
    private final Dao    dao;

    public Job(String name, int maxWaitTime, String drivername, String url, String username, String password) {
        this.name = name;
        this.maxWaitTime = maxWaitTime;
        this.dao = new Dao(drivername, url, username, password);
    }

    public void run() {
        LOG.info("{} started at {}", name, LocalDateTime.now().toString());
        try {
            LOG.info("Database date & time {}", dao.getDatabaseDateTime());
            Thread.sleep(random.nextInt(maxWaitTime));
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }
        LOG.info("{} stopped at {}", name, LocalDateTime.now().toString());
    }

}
