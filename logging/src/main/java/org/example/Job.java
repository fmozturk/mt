package org.example;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.time.LocalDateTime;
import java.util.Random;

/**
 * A class that simulates a job,
 * prints job start time,
 * wait for a random of time,
 * prints job end time
 */
public class Job implements Runnable {

    private final Logger LOG = LoggerFactory.getLogger(Job.class);

    private final Random random = new Random();
    private final String name;
    private final int    maxWaitTime;

    public Job(String name, int maxWaitTime) {
        this.name        = name;
        this.maxWaitTime = maxWaitTime;
    }

    public void run() {
        LOG.info("{} started at {}", name, LocalDateTime.now().toString());
        try {
            Thread.sleep(random.nextInt(maxWaitTime));
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }
        LOG.info("{} stopped at {}", name, LocalDateTime.now().toString());
    }

}
